AOS.init();

nav = document.querySelector('nav');
logo = document.getElementById('logo');
headerContainer = document.querySelector('.header-container');
window.addEventListener('scroll', () => {
    if(window.scrollY > 380) {
        nav.classList.add('nav-fixed');
        logo.src = "https://3841ex3t1et33uno5h3thxr7-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/logo-dark.webp";
        headerContainer.style.opacity = 0;
    } else {
        nav.classList.remove('nav-fixed');
        logo.src = "https://3841ex3t1et33uno5h3thxr7-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/logo-light.png";
        headerContainer.style.opacity = 1;
    }
})

var swiper = new Swiper(".mySwiper", {
    slidesPerView: 3,
    spaceBetween: 30,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    }
  });